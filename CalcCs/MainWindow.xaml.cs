﻿using CalcCs.Model;
using CalcCs.Util;
using System;
using System.Windows;


namespace CalcCs
{
    public partial class MainWindow : Window
    {
        //Used Variables 
        private OperatorEnum currentOperator;
        private decimal answer;

        public bool hasDecimalSigniture = false,
                    isDescriptionBusy = false,
                    isMainDisplayBusy = false;

        //Main Module
        public MainWindow() => this.InitializeComponent();

        #region Numeric Buttons
        private void BtnOne_Click(object sender, RoutedEventArgs e) => this.OnNumericBtnPress("1");

        private void BtnTwo_Click(object sender, RoutedEventArgs e) => this.OnNumericBtnPress("2");

        private void BtnThree_Click(object sender, RoutedEventArgs e) => this.OnNumericBtnPress("3");

        private void BtnFour_Click(object sender, RoutedEventArgs e) => this.OnNumericBtnPress("4");

        private void BtnFive_Click(object sender, RoutedEventArgs e) => this.OnNumericBtnPress("5");

        private void BtnSix_Click(object sender, RoutedEventArgs e) => this.OnNumericBtnPress("6");

        private void BtnSeven_Click(object sender, RoutedEventArgs e) => this.OnNumericBtnPress("7");

        private void BtnEight_Click(object sender, RoutedEventArgs e) => this.OnNumericBtnPress("8");

        private void BtnNine_Click(object sender, RoutedEventArgs e) => this.OnNumericBtnPress("9");

        private void BtnZero_Click(object sender, RoutedEventArgs e) => this.OnNumericBtnPress("0");

        #endregion

        #region Displays Funcs
        private void SetDiscriptionDisplay(string text) => this.disBox.Text = text;

        private void AppendToDiscriptionDisplay(string text) => this.SetDiscriptionDisplay(this.GetDiscriptionDisplay() + text);

        private string GetDiscriptionDisplay() => this.disBox.Text;

        private void SetMainDisplay(string text) => this.tBox.Text = text;

        private void AppendToMainDisplay(string text) => this.SetMainDisplay(this.GetMainDisplay() + text);

        private string GetMainDisplay() => this.tBox.Text;

        private bool IsMaxLeghthPassed() => this.tBox.Text.Length > 18;

        private void Reset()
        {
            this.answer = 0;
            this.currentOperator = OperatorEnum.NONE;
            this.isMainDisplayBusy = false;
            this.hasDecimalSigniture = false;
            this.ClearMainDisplayIfNoPeratorPending();
            this.ClearDescriptionIfNoOperatorPending();
        }

        private void ClearDescriptionIfNoOperatorPending()
        {
            if (this.currentOperator == OperatorEnum.NONE)
            {
                this.SetDiscriptionDisplay(string.Empty);
            }
        }

        private void ClearMainDisplayIfNoPeratorPending()
        {
            if (this.currentOperator == OperatorEnum.NONE)
            {
                this.SetMainDisplay("0");
            }
        }
        #endregion

        #region Action Events
        private void OnNumericBtnPress(string numberStr)
        {
            if (!this.IsMaxLeghthPassed())
            {
                if (this.isMainDisplayBusy)
                {
                    this.AppendToMainDisplay(numberStr);
                }
                else
                {
                    this.ClearDescriptionIfNoOperatorPending();
                    this.SetMainDisplay(numberStr);
                    this.isMainDisplayBusy = true;
                }
                this.AppendToDiscriptionDisplay(numberStr);
            }
        }

        private void OnOperatorBtnPress(OperatorEnum @operator)
        {
            this.hasDecimalSigniture = false;
            string operatorSigniture = Utility.GetOperatorSigniture(@operator);
            if (this.currentOperator != OperatorEnum.NONE)
            {
                this.br(this.currentOperator);
                this.SetDiscriptionDisplay($"{this.GetMainDisplay()} {operatorSigniture} ");
            }
            else
            {
                this.answer = Convert.ToDecimal(this.GetMainDisplay());
                this.isMainDisplayBusy = false;
                if (this.isDescriptionBusy)
                {
                    this.SetDiscriptionDisplay($"{this.GetMainDisplay()} {operatorSigniture} ");
                }
                else
                {
                    this.AppendToDiscriptionDisplay($" {operatorSigniture} ");
                }
            }
            this.currentOperator = @operator;
        }

        private void OnDecimalPointBtnPress()
        {
            if (!this.IsMaxLeghthPassed() && !this.hasDecimalSigniture)
            {
                if (this.isMainDisplayBusy)
                {
                    this.AppendToMainDisplay(".");
                }
                else
                {
                    this.ClearDescriptionIfNoOperatorPending();
                    this.AppendToMainDisplay("0.");
                    this.isMainDisplayBusy = true;
                }
                this.hasDecimalSigniture = true;
                this.AppendToDiscriptionDisplay(".");
            }
        }
        #endregion

        #region Operator Buttons
        private void BtnPoint_Click(object sender, RoutedEventArgs e) => this.OnDecimalPointBtnPress();

        private void BtnPlus_Click(object sender, RoutedEventArgs e) => this.OnOperatorBtnPress(OperatorEnum.ADDITION);

        private void BtnMines_Click(object sender, RoutedEventArgs e) => this.OnOperatorBtnPress(OperatorEnum.SUBSTRACTION);

        private void BtnCross_Click(object sender, RoutedEventArgs e) => this.OnOperatorBtnPress(OperatorEnum.MULTIPLICATION);

        private void BtnDiv_Click(object sender, RoutedEventArgs e) => this.OnOperatorBtnPress(OperatorEnum.DIVISION);

        private void BtnSqr_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double root = Convert.ToDouble(this.GetMainDisplay());
                this.answer = Convert.ToDecimal(Math.Sqrt(root));
                this.SetMainDisplay(this.answer.ToString());
                this.hasDecimalSigniture = false;
                this.isMainDisplayBusy = false;
                this.isDescriptionBusy = true;
                this.SetDiscriptionDisplay($"Sqr({root}) = {this.answer}");
            }
            catch (Exception)
            {
                MessageBox.Show("Error!");
                this.Reset();
            }
        }

        private void BtnPower_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double root = Convert.ToDouble(this.GetMainDisplay());
                this.answer = Convert.ToDecimal(Math.Pow(root, 2));
                this.SetMainDisplay(this.answer.ToString());
                this.hasDecimalSigniture = false;
                this.isMainDisplayBusy = false;
                this.isDescriptionBusy = true;
                this.SetDiscriptionDisplay($"Pow({root} , 2) = {this.answer}");
            }
            catch (Exception)
            {
                MessageBox.Show("Error!");
                this.Reset();
            }
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e) => this.Reset();

        private void BtnEqual_Click(object sender, RoutedEventArgs e)
        {
            this.br(this.currentOperator);
            this.isMainDisplayBusy = false;
            this.currentOperator = OperatorEnum.NONE;
            if (!this.isDescriptionBusy)
            {
                this.AppendToDiscriptionDisplay($" = {this.GetMainDisplay()}");
                this.isDescriptionBusy = true;
            }
        }
        #endregion

        // Keyboard Events 
        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            this.btnEqual.Focus();
            switch (e.Key)
            {
                case System.Windows.Input.Key.Delete:
                    this.Reset();
                    break;
                case System.Windows.Input.Key.NumPad0:
                case System.Windows.Input.Key.D0:
                    this.OnNumericBtnPress("0");
                    break;
                case System.Windows.Input.Key.NumPad1:
                case System.Windows.Input.Key.D1:
                    this.OnNumericBtnPress("1");
                    break;
                case System.Windows.Input.Key.NumPad2:
                case System.Windows.Input.Key.D2:
                    this.OnNumericBtnPress("2");
                    break;
                case System.Windows.Input.Key.NumPad3:
                case System.Windows.Input.Key.D3:
                    this.OnNumericBtnPress("3");
                    break;
                case System.Windows.Input.Key.NumPad4:
                case System.Windows.Input.Key.D4:
                    this.OnNumericBtnPress("4");
                    break;
                case System.Windows.Input.Key.NumPad5:
                case System.Windows.Input.Key.D5:
                    this.OnNumericBtnPress("5");
                    break;
                case System.Windows.Input.Key.NumPad6:
                case System.Windows.Input.Key.D6:
                    this.OnNumericBtnPress("6");
                    break;
                case System.Windows.Input.Key.NumPad7:
                case System.Windows.Input.Key.D7:
                    this.OnNumericBtnPress("7");
                    break;
                case System.Windows.Input.Key.NumPad8:
                case System.Windows.Input.Key.D8:
                    this.OnNumericBtnPress("8");
                    break;
                case System.Windows.Input.Key.NumPad9:
                case System.Windows.Input.Key.D9:
                    this.OnNumericBtnPress("9");
                    break;
                case System.Windows.Input.Key.Add:
                    this.OnOperatorBtnPress(OperatorEnum.ADDITION);
                    break;
                case System.Windows.Input.Key.Subtract:
                    this.OnOperatorBtnPress(OperatorEnum.SUBSTRACTION);
                    break;
                case System.Windows.Input.Key.Multiply:
                    this.OnOperatorBtnPress(OperatorEnum.MULTIPLICATION);
                    break;
                case System.Windows.Input.Key.Divide:
                    this.OnOperatorBtnPress(OperatorEnum.DIVISION);
                    break;
                case System.Windows.Input.Key.Decimal:
                case System.Windows.Input.Key.OemPeriod:
                    this.OnDecimalPointBtnPress();
                    break;
                default:
                    break;
            }
        }

        private void TBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e) => this.isDescriptionBusy = false;

        //External Functions
        private void br(OperatorEnum @operator)
        {
            this.hasDecimalSigniture = false;
            try
            {
                switch (@operator)
                {
                    case OperatorEnum.ADDITION:
                        this.answer += Convert.ToDecimal(this.tBox.Text);
                        this.tBox.Text = Convert.ToString(this.answer);
                        this.isMainDisplayBusy = false;
                        break;
                    case OperatorEnum.SUBSTRACTION:
                        this.answer -= Convert.ToDecimal(this.tBox.Text);
                        this.tBox.Text = Convert.ToString(this.answer);
                        this.isMainDisplayBusy = false;
                        break;
                    case OperatorEnum.MULTIPLICATION:
                        this.answer *= Convert.ToDecimal(this.tBox.Text);
                        this.tBox.Text = Convert.ToString(this.answer);
                        this.isMainDisplayBusy = false;
                        break;
                    case OperatorEnum.DIVISION:
                        this.answer /= Convert.ToDecimal(this.tBox.Text);
                        this.tBox.Text = Convert.ToString(this.answer);
                        this.isMainDisplayBusy = false;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error!");
                this.Reset();
            }

        }
    }
}