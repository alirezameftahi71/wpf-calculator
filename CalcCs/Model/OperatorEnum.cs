﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcCs.Model
{
    public enum OperatorEnum
    {
        NONE,
        ADDITION,
        SUBSTRACTION,
        MULTIPLICATION,
        DIVISION
    }
}
