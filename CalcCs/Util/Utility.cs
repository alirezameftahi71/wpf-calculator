﻿using CalcCs.CustomException;
using CalcCs.Model;

namespace CalcCs.Util
{
    public static class Utility
    {
        public static string GetOperatorSigniture(OperatorEnum @operator)
        {
            switch (@operator)
            {
                case OperatorEnum.ADDITION:
                    return "+";
                case OperatorEnum.SUBSTRACTION:
                    return "-";
                case OperatorEnum.MULTIPLICATION:
                    return "*";
                case OperatorEnum.DIVISION:
                    return "/";
                default:
                    throw new OperatorNotSupportedException(@operator.ToString());
            }
        }
    }
}
